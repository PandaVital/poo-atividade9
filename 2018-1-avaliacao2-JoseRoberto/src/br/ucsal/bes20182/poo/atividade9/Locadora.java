package br.ucsal.bes20182.poo.atividade9;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Locadora {

	static List<Veiculo> veiculos = new ArrayList<Veiculo>();

	public static void cadastrarOnibus(Onibus onibus) {
		veiculos.add(onibus);
		//TODO falta tratar exeption

	}

	public static void cadastrarCaminhao(Caminhao caminhao) {
		veiculos.add(caminhao);
		//TODO falta tratar exeption
	}

	public static void veiculoOrdenarPorValor() {
		Collections.sort(veiculos);
	}

	public static void veiculoOrdenarPorPlaca() {
		Collections.sort(veiculos, new Comparator<Veiculo>() {
			@Override
			public int compare(Veiculo veiculo1, Veiculo veiculo2) {
				int resultado = veiculo1.getPlaca().compareTo(veiculo2.getPlaca());
				if (resultado == 0) {
					resultado = veiculo1.getPlaca().compareTo(veiculo2.getPlaca());
				}
				return resultado;
			}
		});
	}

	public static void veiculoOrdenarPorModelo() {
		Collections.sort(veiculos, new Comparator<Veiculo>() {
			@Override
			public int compare(Veiculo veiculo1, Veiculo veiculo2) {
				int resultado = veiculo1.getModelo().compareTo(veiculo2.getModelo());
				if (resultado == 0) {
					resultado = veiculo1.getModelo().compareTo(veiculo2.getModelo());
				}
				return resultado;
			}
		});
	}

}

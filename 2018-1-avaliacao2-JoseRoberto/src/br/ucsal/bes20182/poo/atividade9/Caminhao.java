package br.ucsal.bes20182.poo.atividade9;

public class Caminhao extends Veiculo {

	private Integer carga;

	public Caminhao(String placa, String modelo, Integer anoDeFabricacao, Float valor, Integer carga) {
		super(placa, modelo, anoDeFabricacao, valor);
		this.carga = carga;
	}

	@Override
	public String toString() {
		return "Caminhao [carga=" + carga + "]";
	}

	public Integer getCarga() {
		return carga;
	}

	public void setCarga(Integer carga) {
		this.carga = carga;
	}

	@Override
	public void setValor(Float valor) {
		this.valor = (float) (carga * 8);
	}

}

package br.ucsal.bes20182.poo.atividade9;

public class Onibus extends Veiculo{

	private Integer maxPassageiros;

	public Onibus(String placa, String modelo, Integer anoDeFabricacao, Float valor, Integer maxPassageiros) {
		super(placa, modelo, anoDeFabricacao, valor);
		this.maxPassageiros = maxPassageiros;
	}

	public Integer getMaxPassageiros() {
		return maxPassageiros;
	}

	public void setMaxPassageiros(Integer maxPassageiros) {
		this.maxPassageiros = maxPassageiros;
	}

	@Override
	public String toString() {
		return "Onibus [maxPassageiros=" + maxPassageiros + "]";
	}
	
	@Override
	public void setValor(Float valor) {
		this.valor = (float) (maxPassageiros*10);
	}
	
}

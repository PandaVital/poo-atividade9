package br.ucsal.bes20182.poo.atividade9;

public class Veiculo implements Comparable<Veiculo> {

	private String placa;

	private String modelo;

	private Integer anoDeFabricacao;

	protected Float valor;

	public Veiculo(String placa, String modelo, Integer anoDeFabricacao, Float valor) {
		super();
		this.placa = placa;
		this.modelo = modelo;
		this.anoDeFabricacao = anoDeFabricacao;
		this.valor = valor;
	}

	public String getPlaca() {
		return placa;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public Integer getAnoDeFabricacao() {
		return anoDeFabricacao;
	}

	public void setAnoDeFabricacao(Integer anoDeFabricacao) {
		this.anoDeFabricacao = anoDeFabricacao;
	}

	public Float getValor() {
		return valor;
	}

	public void setValor(Float valor) {
		this.valor = valor;
	}

	@Override
	public String toString() {
		return "Veiculo [placa=" + placa + ", modelo=" + modelo + ", anoDeFabricacao=" + anoDeFabricacao + ", valor="
				+ valor + "]";
	}

	@Override
	public int compareTo(Veiculo outroveiculo) {
		return valor.compareTo(outroveiculo.getValor());
	}
}
